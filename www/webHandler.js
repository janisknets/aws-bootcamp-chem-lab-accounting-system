import fs from "fs";

export default async (event) => {
  let contentPath;
  let contentType = "text/html";
  switch (event.path) {
    case "":
      contentPath = "/html/index.html";
      break;
    case "/menu":
      contentPath = "/html/menu.html";
      break;
    case "/login":
      contentPath = "/html/login.html";
      break;
    case "/inventory":
      contentPath = "/html/inventory.html";
      break;
    case "/inventory/add":
      contentPath = "/html/inventoryAdd.html";
      break;
    case "/chemicals":
      contentPath = "/html/chemicals.html";
      break;
    case "/chemicals/add":
      contentPath = "/html/chemicalsAdd.html";
      break;
    case "/css/button.css":
    case "/css/front_page.css":
    case "/css/header.css":
    case "/css/main.css":
    case "/css/normalize.css":
    case "/css/table.css":
      contentType = "text/css";
      contentPath = event.path;
      break;
    case "/img/favicon.ico":
      contentType = "image/x-icon";
      contentPath = event.path;
      break;
    case "/img/logo.svg":
      contentType = "image/svg+xml";
      contentPath = event.path;
      break;
    case "/js/main.js":
      contentPath = event.path;
      contentType = "application/javascript";
      break;
    default:
        break;
  }

  if (contentType !== "image/x-icon") {
    const content = fs.readFileSync(`${process.cwd()}/www${contentPath}`, {encoding:'utf8', flag:'r'});
    return {
      statusCode: 200,
      headers: {
          "Content-Type": contentType
      },
      body: content
    };
  } else {
    const content = fs.readFileSync(`${process.cwd()}/www${contentPath}`);
    return {
      statusCode: 200,
      headers: {
          "Content-Type": contentType
      },
      body: content
    };

  }
};
